﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SettingsManager : MonoBehaviour {

	public static SettingsManager manager;

	public GameObject colorChooser;
	public GameObject settings;
	public GameObject modeSelector;
	public Image image;
	public GameObject ball;

	void OnEnable() {
		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}

	void OnDisable() {
		SceneManager.sceneLoaded -= OnLevelFinishedLoading;
	}

	void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode) {
		if (ball == null && Camera.main.GetComponentInChildren<Renderer> () != null) {
			ball = Camera.main.GetComponentInChildren<Renderer> ().gameObject;
		} else {
			return;
		}

		if (ball != null) {
			ball.GetComponent<Renderer> ().material.color = image.color;
		}
	}

	void Awake() {
		if (manager == null) {
			DontDestroyOnLoad (gameObject);	
			manager = this;
		} else if (manager != this) {
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		colorChooser.SetActive (false);
		settings.SetActive (true);
		modeSelector.SetActive (false);
		if (ball == null && Camera.main.GetComponentInChildren<Renderer> () != null) {
			ball = Camera.main.GetComponentInChildren<Renderer> ().gameObject;
		} else {
			return;
		}
		if (ball != null) {
			image.color = ball.GetComponent<Renderer> ().material.color;
		} else {
			Debug.LogWarning("SettingsManager: no balls are atached to the script; can be ignored if no balls are visible from controller");
		}
	}

	public void SelectColor() {
		Color color = EventSystem.current.currentSelectedGameObject.GetComponent<Image>().color;
		image.color = color;
		if (ball != null) {
			ball.GetComponent<Renderer> ().material.color = color;
		}
	}

	public void LoadColorSelector() {
		if (colorChooser.activeSelf) { //color was active
			SceneSetup.isActive = true;
		} else {
			SceneSetup.isActive = false;
			if (settings.activeSelf) { //settings was active
				settings.SetActive(false);
				if (modeSelector.activeSelf) {
					modeSelector.SetActive (false);
				}
			}
		}
		colorChooser.SetActive (!colorChooser.activeSelf); // inverse colorchooser
	}

	public void LoadSettings() {
		if (!settings.activeSelf) {
			settings.SetActive (!settings.activeSelf);
			SceneSetup.isActive = false;
		} else {
			if (modeSelector.activeSelf) {
				modeSelector.SetActive (!modeSelector.activeSelf);
			} else {
				settings.SetActive (!settings.activeSelf);
				SceneSetup.isActive = true;
			}
		}
	}
}
