﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Tapper : MonoBehaviour {

	private SimplifySocket ss;
	public string socketId;

	public bool isRotationLock;

	public float maxExplosionSize = 3;
	public GameObject chargeExplosionVisual;
	public Transform world;
	private bool shotStarted = false;

	private float shotTime;

	private float dist;
	private Vector3 v3Offset;
	private Plane plane;
	private Vector3 origin;

	private IEnumerator coroutine;

	public Image colorSwatch;

	// Use this for initialization
	void Start () {
		ss = GetComponent<SimplifySocket> ();
		world = WorldManager.manager.transform;
		colorSwatch = SettingsManager.manager.image;
		chargeExplosionVisual.SetActive (false);
		origin = chargeExplosionVisual.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (SceneSetup.isActive && Input.touchCount > 0) {
			Vector3 touch = Input.GetTouch (0).position;
			if (!shotStarted && Input.GetTouch (0).phase == TouchPhase.Began) {
				chargeExplosionVisual.SetActive (true);
				chargeExplosionVisual.transform.localScale = new Vector3 (0f, 0f, 0f);

				//place ball under finger
				plane.SetNormalAndPosition (Camera.main.transform.forward, origin);
				Ray ray = Camera.main.ScreenPointToRay (touch);
				float dist;
				plane.Raycast (ray, out dist);
				v3Offset = ray.GetPoint (dist); 
				Vector3 startV3 = ray.GetPoint (dist) + v3Offset;

				shotStarted = true;
				shotTime = Time.time;
			}

			if (shotStarted && Input.GetTouch (0).phase == TouchPhase.Moved) {
				//place ball under finger
				Ray ray = Camera.main.ScreenPointToRay (touch);
				float dist;
				plane.Raycast (ray, out dist);
				Vector3 v3Pos = ray.GetPoint (dist);
				chargeExplosionVisual.transform.position = v3Pos + v3Offset;

				//increase size of ball
				if (chargeExplosionVisual.transform.localScale.magnitude < maxExplosionSize) {
					float currentSize = Time.time - shotTime;
					chargeExplosionVisual.transform.localScale = new Vector3 (currentSize, currentSize, currentSize);
				}
			}

			if(shotStarted && Input.GetTouch (0).phase == TouchPhase.Ended){
				//hide ball
				chargeExplosionVisual.SetActive(false);

				touch.z = 50;
				// Phone direction
				Vector3 phoneDirection = Camera.main.ScreenToWorldPoint(touch);
				Debug.DrawRay (Camera.main.transform.position, phoneDirection, Color.blue, 3f);

				// World direction
				Vector3 worldDirection = world.rotation * -Vector3.forward;
				Debug.DrawRay (Camera.main.transform.position, worldDirection, Color.red, 3f);

				// make phone direction relative to world adjustment
				float angle = Vector3.Angle (worldDirection, -Vector3.forward);
				Vector3 cross = Vector3.Cross(worldDirection, -Vector3.forward);
				if (cross.y > 0) {
					angle = 360 - angle;
				}
				float size = Mathf.Clamp (Time.time - shotTime, 0f, maxExplosionSize);
				Vector3 worldPhoneDirection = Quaternion.AngleAxis (angle, -Vector3.up) * phoneDirection; 
				Debug.DrawRay (Camera.main.transform.position, worldPhoneDirection, Color.magenta, 3f);

				Debug.Log (worldPhoneDirection);
				//create JSON Object
				JSONObject jPos = new JSONObject(JSONObject.Type.ARRAY);
				jPos.AddField ("x", worldPhoneDirection.x);
				jPos.AddField ("y", worldPhoneDirection.y);
				jPos.AddField ("z", worldPhoneDirection.z);

				JSONObject json = new JSONObject (JSONObject.Type.OBJECT);
				json.AddField ("position", jPos);
				json.AddField ("size", size);
				json.AddField ("shotType", "tap");
				
				ss.SendDataToHost(socketId, json);
				shotStarted = false;
			}
		}
	}

	public void SetSocketId(string newSocketId) {
		socketId = newSocketId;
	}

	public void ToggleLock (bool locked) {
		if (locked) {
			world.transform.rotation = Quaternion.Euler (0, 0, 0);
		}
	}
}
