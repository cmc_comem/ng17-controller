﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldManager : MonoBehaviour {

	public static WorldManager manager;

	void Awake() {
		if (manager == null) {
			DontDestroyOnLoad (gameObject);	
			manager = this;
		} else if (manager != this) {
			Destroy (gameObject);
		}
	}
}
