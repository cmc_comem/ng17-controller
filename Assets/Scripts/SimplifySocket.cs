﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using SocketIO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SimplifySocket : MonoBehaviour {

	/**********************************************
	 *              	Attributes 	              *
	 **********************************************/

	// Public
	public bool connectToLocal = false;
	public SocketIOComponent sock;
	public Dictionary<string, GameObject> players;
	public int maxConnectedPlayers = 0;
	public static SimplifySocket instance;

	//Private
	private Clicker clicker;
	private Swiper swiper;
	private Angry angry;
	private Tapper tapper;


	/**********************************************
	 *              Public methods                *
	 **********************************************/


	/**********************************************
	 *                 Lifecycle                  *
	 **********************************************/

	public void Awake(){

		Debug.Log ("Simplify socket being called");
		sock = GetComponent<SocketIOComponent> ();
		sock.url = "ws://ng2.comem.ch/socket.io/?EIO=4&transport=websocket"; // OVH: 149.202.223.139  DO: 46.101.152.216

		//todo - temporary remove
		//#if UNITY_EDITOR
		if (connectToLocal) {
			sock.url = "ws://localhost:3010/socket.io/?EIO=4&transport=websocket";
		} 
		//#endif
		sock.autoConnect = true;

	}

	void Start () {
		instance = this;
		clicker = GetComponent<Clicker> ();
		swiper = GetComponent<Swiper> ();
		angry = GetComponent<Angry> ();
		tapper = GetComponent<Tapper> ();
		players = new Dictionary<string, GameObject> ();
		sock.On ("open", OnSocketOpen);
		sock.On ("msg", OnGotMessageFromClient);
		//sock.On ("reconnect", OnNodeServerDisconnect);
		// on disconnect
		sock.Connect ();
	}

	private int PlayerCount(){
		return GameObject.FindGameObjectsWithTag ("Player").Length;
	}

	public int ActivePlayers() {
		return players.Count;
	}

	private void OnGotMessageFromClient(SocketIOEvent ev){
		var sockId = ev.data ["senderId"].str;
		Debug.Log (sockId);
	}

	public void OnSocketOpen(SocketIOEvent ev){
		Debug.Log("updated socket id " + sock.sid);
		// Double connect for some reason
		if (sock.sid.Length > 0) {
			if (clicker != null) {
				clicker.SetSocketId (sock.sid);
			} else if (swiper != null) {
				swiper.SetSocketId (sock.sid);
			} else if(angry != null){
				angry.SetSocketId (sock.sid);
			} else {
				tapper.SetSocketId (sock.sid);
			}
			Identify ();
		}
	}

	private void Identify(){
		Dictionary<string, string> data = new Dictionary<string, string>();
		data["type"] = "client";
		sock.Emit("identify", new JSONObject(data));
	}

	void HandleSocketDisconnect(string sockId){
		// Simple for now, maybe create a more advanced system later; Don't think it gets simpler than that!
	}

	public void SendDataToHost(string socketId, JSONObject data){
		if (socketId.Length > 0) {
			sock.Emit("msg", data);
		} else {
			Debug.Log ("no socket id");
		}
	}

	void Broadcast(JSONObject data){
		// Sends message to all the controllers
		data.AddField ("destinationId", "broadcast");
		sock.Emit("msg", data);
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.B)) {
			Dictionary<string, string> d = new Dictionary<string, string>();
			d["test"] = "test";
			Broadcast (new JSONObject(d));
			Debug.Log ("broadcast");
		} 
	}
}