﻿using UnityEngine;
using System.Collections;
using SocketIO;

/**
 * Handles messages from client socket
 */

public delegate void SendDataToController(string sockId, JSONObject data);

public interface ClientSocketHandler {

	void HandleMessage (JSONObject msg);
	void HandleDisconnect ();
	SendDataToController OnNeedsSendDataToController {get; set;}
	string SocketId { get; set;}

}
