﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ModeManager : MonoBehaviour {

	public void LoadLevelAsync(string sceneName) {
		SceneManager.LoadSceneAsync (sceneName);
	}
}
