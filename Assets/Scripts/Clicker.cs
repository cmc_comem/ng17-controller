﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Clicker : MonoBehaviour {

	private SimplifySocket ss;
	public string socketId;

	public bool isRotationLock;

	public Transform world;
	private bool shooterLock = false;

	public Image colorSwatch;

	// Use this for initialization
	void Start () {
		ss = GetComponent<SimplifySocket> ();
		world = WorldManager.manager.transform;
		colorSwatch = SettingsManager.manager.image;
	}
	
	// Update is called once per frame
	void Update () {
		if(!shooterLock && Input.touchCount > 0 && Input.GetTouch(0).phase != TouchPhase.Moved){
			shooterLock = true;

			Vector3 touch = Input.GetTouch (0).position;
			touch.z = 50;
			// Phone direction
			Vector3 phoneDirection = Camera.main.ScreenToWorldPoint(touch);
			Debug.DrawRay (Camera.main.transform.position, phoneDirection * 30, Color.blue, 3f);

			// World direction
			Vector3 worldDirection = world.rotation * -Vector3.forward;
			Debug.DrawRay (Camera.main.transform.position, worldDirection * 30, Color.red, 3f);

			// make phone direction relative to world adjustment
			float angle = Vector3.Angle (worldDirection, -Vector3.forward);
			Vector3 cross = Vector3.Cross(worldDirection, -Vector3.forward);
			if (cross.y > 0) {
				angle = 360 - angle;
			}

			Vector3 worldPhoneDirection = Quaternion.AngleAxis (angle, -Vector3.up) * phoneDirection; 
			Debug.DrawRay (Camera.main.transform.position, worldPhoneDirection * 30, Color.magenta, 3f);
			
			//create JSON Object
			JSONObject jPos = new JSONObject(JSONObject.Type.ARRAY);
			jPos.AddField ("x", worldPhoneDirection.x);
			jPos.AddField ("y", worldPhoneDirection.y);
			jPos.AddField ("z", worldPhoneDirection.z);

			Color ballColor = colorSwatch.color;
			JSONObject jColor = new JSONObject (JSONObject.Type.ARRAY);
			jColor.AddField ("r", ballColor.r);
			jColor.AddField ("g", ballColor.g);
			jColor.AddField ("b", ballColor.b);

			JSONObject json = new JSONObject (JSONObject.Type.OBJECT);
			json.AddField ("color", jColor);
			json.AddField ("position", jPos);
			json.AddField ("shotType", "target");
			
			ss.SendDataToHost(socketId, json);
			Invoke ("ShooterUnlock", 2f);
		}
	}

	public void SetSocketId(string newSocketId) {
		socketId = newSocketId;
	}

	public void ToggleLock (bool locked) {
		if (locked) {
			world.transform.rotation = Quaternion.Euler (0, 0, 0);
		}
	}

	private void ShooterUnlock() {
		shooterLock = false;
	}
}
