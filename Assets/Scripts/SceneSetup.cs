﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneSetup : MonoBehaviour {

	public Transform world;
	public Transform cam;

	public GameObject panel;
	public GameObject modeSelector;

	public static bool isActive = false;

	void OnEnable() {
		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}

	void OnDisable() {
		SceneManager.sceneLoaded -= OnLevelFinishedLoading;
	}

	void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode) {
		cam = Camera.main.transform;
		ToggleMode ();
	}

	// Use this for initialization
	void Start () {
		if (world == null) {
			Debug.LogError ("no world transform");
			world = GameObject.Find ("World").transform;
		}
		if (cam == null) {
			Debug.LogError ("no camera transform");
			cam = Camera.main.transform;
		}
		if (panel == null) {
			Debug.LogError ("Menu: no panel! ADD IT!!!");
		}
		if (modeSelector == null) {
			Debug.LogError ("Menu: No mode selector! ADD IT!!!!!");
		}

	}

	void Update() {
		if (modeSelector.activeSelf) {
			if (Input.GetKeyDown (KeyCode.Escape)) {
				ToggleMode ();
			}
		}
	}

	public void SpinScene() {
		float yRot =  cam.rotation.eulerAngles.y - 180f;

		world.rotation = Quaternion.Euler (new Vector3(world.rotation.eulerAngles.x, yRot, world.rotation.eulerAngles.z));
	}

	public void Ready() {
		panel.SetActive (false);
		isActive = true;
	}

	public void ToggleMode() {
		modeSelector.SetActive (!modeSelector.activeSelf);
		switch(SceneManager.GetActiveScene().name) {
		case "Gyro swipe":
			modeSelector.transform.GetChild (0).GetComponent<Button> ().interactable = false;
			modeSelector.transform.GetChild (1).GetComponent<Button> ().interactable = true;
			modeSelector.transform.GetChild (2).GetComponent<Button> ().interactable = true;
			modeSelector.transform.GetChild (3).GetComponent<Button> ().interactable = true;
			break;
		case "Gyro touch":
			modeSelector.transform.GetChild (0).GetComponent<Button> ().interactable = true;
			modeSelector.transform.GetChild (1).GetComponent<Button> ().interactable = false;
			modeSelector.transform.GetChild (2).GetComponent<Button> ().interactable = true;
			modeSelector.transform.GetChild (3).GetComponent<Button> ().interactable = true;
			break;
		case "Gyro angry":
			modeSelector.transform.GetChild (0).GetComponent<Button> ().interactable = true;
			modeSelector.transform.GetChild (1).GetComponent<Button> ().interactable = true;
			modeSelector.transform.GetChild (2).GetComponent<Button> ().interactable = false;
			modeSelector.transform.GetChild (3).GetComponent<Button> ().interactable = true;
			break;
		case "Gyro tap":
			modeSelector.transform.GetChild (0).GetComponent<Button> ().interactable = true;
			modeSelector.transform.GetChild (1).GetComponent<Button> ().interactable = true;
			modeSelector.transform.GetChild (2).GetComponent<Button> ().interactable = true;
			modeSelector.transform.GetChild (3).GetComponent<Button> ().interactable = false;
			break;
		default:
			break;
		}
	}

	public void ToggleRotation() {
		bool isUnlocked = !Camera.main.GetComponent<GyroController> ().isRotationLock;
		Camera.main.GetComponent<GyroController> ().isRotationLock = isUnlocked;
		switch(SceneManager.GetActiveScene().name) {
		case "Gyro swipe":
			Camera.main.GetComponent<Swiper> ().ToggleLock (isUnlocked);
			break;
		case "Gyro touch":
			Camera.main.GetComponent<Clicker> ().ToggleLock (isUnlocked);
			break;
		case "Gyro angry":
			Camera.main.GetComponent<Angry> ().ToggleLock (isUnlocked);
			break;
		case "Gyro tap":
			Camera.main.GetComponent<Tapper> ().ToggleLock (isUnlocked);
			break;
		default:
			break;
		}
	}
}
