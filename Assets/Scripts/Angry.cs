﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Angry : MonoBehaviour {

	private SimplifySocket ss;
	public string socketId;

	public bool isRotationLock;

	private bool shooterLock = false;
	private bool shotStarted = false;

	private Vector3 referenceSwipe;
	private Vector3 potentialSwipe;
	private Vector3 startSwipe;
	private Vector3 endSwipe;

	public Transform ball;
	public Transform world;
	public Image colorSwatch;

	private float dist;
	private Vector3 v3Offset;
	private Plane plane;
	private Vector3 origin;

	public LineRenderer lineRenderer;

	public Vector3 directionNorm;

	// Use this for initialization
	void Start () {
		ss = GetComponent<SimplifySocket> ();
		origin = ball.localPosition;
		colorSwatch = SettingsManager.manager.image;
		world = WorldManager.manager.transform;
	}

	// Update is called once per frame
	void Update () {
		if (SceneSetup.isActive && Input.touchCount > 0) {
			if (!shooterLock && Input.GetTouch (0).phase == TouchPhase.Began) {
				shooterLock = true;
				shotStarted = true;
				plane.SetNormalAndPosition (Camera.main.transform.forward, ball.position);

				Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
				float dist;
				plane.Raycast (ray, out dist);
				v3Offset = ball.position - ray.GetPoint (dist); 

				Vector3 startV3 = ray.GetPoint (dist) + v3Offset;
				startSwipe = startV3;

				referenceSwipe = Input.GetTouch (0).position;
			}

			if (shotStarted && Input.GetTouch (0).phase == TouchPhase.Moved) {
				Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
				float dist;
				plane.Raycast (ray, out dist);
				Vector3 v3Pos = ray.GetPoint (dist);
				ball.position = v3Pos + v3Offset;
			}

			if (shotStarted && Input.GetTouch (0).phase == TouchPhase.Ended) {
				shotStarted = false;
				ball.GetComponent<Rigidbody> ().isKinematic = false;

				Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch (0).position);
				float dist;
				plane.Raycast (ray, out dist);
				Vector3 v3Pos = ray.GetPoint (dist);
				ball.position = v3Pos + v3Offset;

				endSwipe = ball.position;
				Vector3 endPosition = Input.GetTouch (0).position;
				Vector3 delta =   endPosition - referenceSwipe;

				float speed = delta.magnitude * 10; // get stretch length

				// Phone direction
				Vector3 phoneDirection = Camera.main.transform.rotation * Vector3.up;
				Debug.DrawRay (Camera.main.transform.position, phoneDirection * 30, Color.blue, 3f);

				// World direction
				Vector3 worldDirection = world.rotation * -Vector3.forward;
				Debug.DrawRay (Camera.main.transform.position, worldDirection * 30, Color.red, 3f);

				// Swipe direction
				Vector3 swipeDirection =  new Vector3 (delta.x, delta.z, delta.y);
				Debug.DrawRay (Camera.main.transform.position, swipeDirection * 30, Color.green, 3f);

				// make phone direction relative to world adjustment
				float angle = Vector3.Angle (worldDirection, -Vector3.forward);
				Vector3 cross = Vector3.Cross(worldDirection, -Vector3.forward);
				if (cross.y > 0) {
					angle = 360 - angle;
				} 

				Vector3 worldPhoneDirection = Quaternion.AngleAxis (angle, -Vector3.up) * phoneDirection; 
				Debug.DrawRay (Camera.main.transform.position, worldPhoneDirection * 30, Color.magenta, 3f);

				// add swipe direction to worldPhone direction
				float swipeAngle = Vector3.Angle (swipeDirection, Vector3.forward);
				Vector3 swipeCross = Vector3.Cross(swipeDirection, Vector3.forward);
				if (swipeCross.y > 0) {
					swipeAngle = 360 - swipeAngle;
				}

				Vector3 swipeAdjustedDirection = Quaternion.AngleAxis (swipeAngle, Vector3.up) * worldPhoneDirection; 
				swipeAdjustedDirection = new Vector3 (-swipeAdjustedDirection.x, swipeAdjustedDirection.y, -swipeAdjustedDirection.z);
				Debug.DrawRay (Camera.main.transform.position, swipeAdjustedDirection * 30, Color.yellow, 3f);

				Vector3 direction = swipeAdjustedDirection;
				directionNorm = direction.normalized;
				direction = direction / Screen.dpi;

				lineRenderer.gameObject.SetActive (true);
				Vector3[] positions = new Vector3[2];
				positions [0] = startSwipe;
				positions [1] = endSwipe;

				lineRenderer.SetPositions(positions);

				//create JSON Object
				JSONObject jDir = new JSONObject (JSONObject.Type.ARRAY);
				jDir.AddField ("x", direction.x);
				jDir.AddField ("y", direction.y);
				jDir.AddField ("z", direction.z);

				Color ballColor = colorSwatch.color;
				JSONObject jColor = new JSONObject (JSONObject.Type.ARRAY);
				jColor.AddField ("r", ballColor.r);
				jColor.AddField ("g", ballColor.g);
				jColor.AddField ("b", ballColor.b);

				JSONObject json = new JSONObject (JSONObject.Type.OBJECT);
				json.AddField ("direction", jDir);
				json.AddField ("velocity", speed);
				json.AddField ("shotType", "swipe");
				json.AddField ("color", jColor);

				ss.SendDataToHost (socketId, json);

				Vector3 localDirection = phoneDirection;
				ball.GetComponent<Rigidbody> ().velocity = localDirection * speed / 20;
				StartCoroutine ("Reset");
			}
		}
	}

	float GetAngleByDeviceAxis(Vector3 axis) {
		Quaternion referenceRotation = Quaternion.identity;
		Quaternion deviceRotation = DeviceRotation.Get();
		Quaternion eliminationOfOthers = Quaternion.Inverse(
			Quaternion.FromToRotation(referenceRotation * axis, deviceRotation * axis)
		);
		Vector3 filteredEuler = (eliminationOfOthers * deviceRotation).eulerAngles;

		float result = filteredEuler.z;
		if (axis == Vector3.up) {
			result = filteredEuler.y;
		}
		if (axis == Vector3.forward) {
			result = filteredEuler.z;
		}
		return result;
	}

	public void SetSocketId(string newSocketId) {
		socketId = newSocketId;
	}

	public void ToggleLock (bool locked) {
		if (locked) {
			world.transform.rotation = Quaternion.Euler (0, 0, 0);
		}
	}

	IEnumerator Reset() {
		yield return new WaitForSeconds (2f);
		shooterLock = false;
		ball.GetComponent<Rigidbody> ().isKinematic = true;
		ball.localPosition = origin;
	}
}
